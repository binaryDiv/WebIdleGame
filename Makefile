# == Makefile ==

# Project directories
SERVERDIR := server
WEBDIR := web


# MAKE TARGETS
# ------------

# Default 'all' target
all: server web

# Server
server:
	$(MAKE) -C $(SERVERDIR)

serverrun:
	$(MAKE) -C $(SERVERDIR) run

serverclean:
	$(MAKE) -C $(SERVERDIR) clean

serverdeploy:
	$(MAKE) -C $(SERVERDIR) deploy

# Web
web:
	$(MAKE) -C $(WEBDIR)

webclean:
	$(MAKE) -C $(WEBDIR) clean

webdeploy:
	$(MAKE) -C $(WEBDIR) deploy

# Deployment
deploy: serverdeploy webdeploy

# Clean generated files
clean: serverclean webclean

.PHONY: all server serverrun serverclean serverdeploy web webclean webdeploy clean deploy

