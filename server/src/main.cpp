// main.cpp -- Server main() function

#include <iostream>

int main() {
	// Hello world.
	std::cout << "Hello. I'm the Server." << std::endl;
	std::cout << "Basically... Run." << std::endl;
	return 0;
}
