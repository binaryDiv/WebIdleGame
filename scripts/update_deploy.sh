#!/bin/sh

# -- OPTIONS

# Git repository directory
SOURCE_GIT_DIR="__GIT_DIR__"

# Deployment target directories
export DEPLOY_SERVER_DIR="__SERVER_DIR__"
export DEPLOY_WEB_DIR="__WWW_DIR__"


# -- FUNCTIONS

die() {
	echo "Error: $@" 1>&2
	exit 1
}


# -- PREPARATION

# Check arguments
if [ $# -gt 2 ]; then
	die "only two arguments allowed (try --help)"
fi

DO_CLEANONLY=
DO_BUILDONLY=
DO_DEPLOY_SERVER=
DO_DEPLOY_WEB=

while [ $# -gt 0 ]; do
	case "$1" in
	--help|-h)
		echo "Usage: $0 [--clean] [--buildonly] [--server|--web]"
		echo "Updates git repository (git pull), builds and deploys (part of) the application."
		echo
		echo "  --buildonly  Only build (make), no deployment"
		echo "  --server     Build and deploy only server"
		echo "  --web        Build and deploy only web client"
		echo "  --clean      ONLY run 'make clean' in repository (DOES NOT pull/build/deploy, ignores all other arguments)"
		echo
		echo "If neither --server nor --web are specified, server AND web client will be build and deployed."
		exit 1
		;;
	
	--buildonly)
		DO_BUILDONLY=1
		shift
		;;
	
	--server)
		DO_DEPLOY_SERVER=1
		shift
		;;
	
	--web)
		DO_DEPLOY_WEB=1
		shift
		;;
	
	--clean)
		DO_CLEANONLY=1
		shift
		;;
	
	*)
		die "unknown argument '$1' (try --help)"
		;;
	esac
done

# Default (no --server or --web specified)
if [ -z "$DO_DEPLOY_SERVER" -a -z "$DO_DEPLOY_WEB" ]; then
	DO_DEPLOY_SERVER=1
	DO_DEPLOY_WEB=1
fi


# -- SOURCE UPDATE

# Change working directory
echo "-- Changing to git directory '$SOURCE_GIT_DIR' ..."
cd "$SOURCE_GIT_DIR" || die "could not change directory"

echo

# Alternative function --clean
if [ "$DO_CLEANONLY" ]; then
	echo "-- Running 'make clean'"
	make clean || die "make clean failed"
	
	echo
	
	echo "-- Cleaning finished."
	exit 0
fi

# Git pull
echo "-- Pulling git repository ..."
git pull || die "git pull failed"

echo


# -- BUILD THINGS

if [ "$DO_DEPLOY_SERVER" ]; then
	# Build server
	echo "-- Building server ..."
	make server || die "make server failed"
	
	echo
fi

if [ "$DO_DEPLOY_WEB" ]; then
	# Build web client
	echo "-- Building web client ..."
	make web || die "make web failed"
	
	echo
fi

# Done, if --buildonly specified
if [ "$DO_BUILDONLY" ]; then
	echo "-- Build finished."
	exit 0
fi


# -- DEPLOY THINGS

if [ "$DO_DEPLOY_SERVER" ]; then
	# Deploy server
	echo "-- Deploying server to '$DEPLOY_SERVER_DIR' ..."
	make serverdeploy || die "make serverdeploy failed"
	
	echo
fi

if [ "$DO_DEPLOY_WEB" ]; then
	# Deploy web client
	echo "-- Deploying web client to '$DEPLOY_WEB_DIR' ..."
	make webdeploy || die "make webdeploy failed"
	
	echo
fi


# -- DONE :)

echo "-- Deployment finished."

