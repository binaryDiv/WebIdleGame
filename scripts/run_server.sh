#!/bin/sh

# -- OPTIONS
SERVER_DIR="__SERVER_DIR__"
SERVER_EXE="ws_server"
SERVER_ARGUMENTS=


# -- FUNCTIONS

die() {
	echo "Error: $@" 1>&2
	exit 1
}


# -- CHANGE DIRECTORY
cd "$SERVER_DIR" || die "could not change working directory to '$SERVER_DIR'"


# -- RUN SERVER
exec "./$SERVER_EXE" $SERVER_ARGUMENTS "$@" || die "could not exec '$SERVER_EXE'"

